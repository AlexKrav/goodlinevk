package com.alexkravcha.goodlinevk.models;

import com.google.gson.annotations.SerializedName;

public class BaseResponse<T> {
    @SerializedName("response")
    private T response;

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }
}
