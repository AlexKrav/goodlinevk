package com.alexkravcha.goodlinevk.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.alexkravcha.goodlinevk.R;
import com.alexkravcha.goodlinevk.base.BaseAdapter;
import com.alexkravcha.goodlinevk.databinding.FriendsItemBinding;
import com.alexkravcha.goodlinevk.models.Friend;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class FriendsAdapter extends BaseAdapter<Friend, FriendsAdapter.FriendsHolder> {
    private OnFriendClickListener onFriendClickListener;

    public FriendsAdapter(OnFriendClickListener onFriendClickListener) {
        this.onFriendClickListener = onFriendClickListener;
    }

    @NonNull
    @Override
    public FriendsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        FriendsItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.friends_item, viewGroup, false);
        return new FriendsHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FriendsHolder holder, int position) {
        Friend friend = getItem(position);
        holder.binding.name.setText(friend.getFirstName());
        Glide.with(holder.binding.profileImage)
                .load(friend.getPhotoLink100())
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .thumbnail(Glide.with(holder.binding.profileImage)
                        .load(friend.getPhotoLink50()))
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .into(holder.binding.profileImage);

        holder.binding.friendItem.setOnClickListener(view -> {
            onFriendClickListener.onFriendClicked(friend);
        });
    }

    class FriendsHolder extends RecyclerView.ViewHolder {
        FriendsItemBinding binding;

        FriendsHolder(@NonNull FriendsItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface OnFriendClickListener {
        void onFriendClicked(Friend friend);
    }
}
