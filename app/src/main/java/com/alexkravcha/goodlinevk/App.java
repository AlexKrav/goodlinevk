package com.alexkravcha.goodlinevk;

import android.app.Application;
import android.content.Context;
import android.view.View;

import androidx.appcompat.app.AppCompatDelegate;

import com.alexkravcha.goodlinevk.di.component.ApplicationComponent;
import com.alexkravcha.goodlinevk.di.component.DaggerApplicationComponent;
import com.alexkravcha.goodlinevk.di.module.ApplicationModule;
import com.vk.sdk.VKSdk;

public class App extends Application {
    public static App instance;
    private static ApplicationComponent applicationComponent;

    public static Context getContext() {
        return instance;
    }

    @Override
    public void onCreate() {
        instance = this;
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        super.onCreate();
        initComponent();
        VKSdk.initialize(this);

    }

    private void initComponent() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();
    }

    public static ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
