package com.alexkravcha.goodlinevk.base;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public abstract class BaseAdapter<M, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {
    private final List<M> items;
    private OnItemClickListener itemClickListener;

    protected BaseAdapter(@NonNull List<M> items) {
        this.items = items;
    }

    protected BaseAdapter() {
        this.items = new ArrayList<>();
    }

    protected static View inflate(@NonNull ViewGroup parent, @LayoutRes int layoutRes) {
        return LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void add(@NonNull M item) {
        this.items.add(item);
        notifyItemInserted(items.size() - 1);
    }

    public void remove(int position) {
        this.items.remove(position);
        notifyItemRemoved(position);
    }

    public void add(@NonNull Collection<M> items) {
        int insertPosition = this.items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(insertPosition, items.size());
    }

    public void replace(@NonNull Collection<M> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public void clear() {
        final int size = this.items.size();
        this.items.clear();
        notifyItemRangeRemoved(0, size);
    }

    public boolean isEmpty() {
        return this.items.isEmpty();
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    @NonNull
    public M getItem(int position) {
        return this.items.get(position);
    }

    @Nullable
    public OnItemClickListener getItemClickListener() {
        return itemClickListener;
    }

    public void setItemClickListener(@Nullable OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    public List<M> getItems() {
        return items;
    }

    public interface OnItemClickListener {
        void onItemClick(@NonNull View view, int position);

        void onProClick();
    }
}