package com.alexkravcha.goodlinevk.base;

import android.os.Bundle;

import androidx.annotation.LayoutRes;

import com.alexkravcha.goodlinevk.App;
import com.alexkravcha.goodlinevk.R;
import com.alexkravcha.goodlinevk.navigation.SupportAppNavigator;
import com.arellomobile.mvp.MvpAppCompatActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;
import ru.terrakok.cicerone.NavigatorHolder;

public abstract class BaseActivity extends MvpAppCompatActivity {

    @Inject
    NavigatorHolder navigatorHolder;

    private SupportAppNavigator navigator = new SupportAppNavigator(this, R.id.fragment_container);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        App.getApplicationComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(provideLayoutResId());
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        super.onPause();
        navigatorHolder.removeNavigator();
    }

    @LayoutRes
    public abstract int provideLayoutResId();
}
