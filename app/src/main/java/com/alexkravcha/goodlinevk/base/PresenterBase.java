package com.alexkravcha.goodlinevk.base;

import androidx.annotation.CallSuper;

import com.arellomobile.mvp.MvpPresenter;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class PresenterBase<View extends ViewBase> extends MvpPresenter<View> {
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    protected void disposeOnDestroy(Disposable d) {
        compositeDisposable.add(d);
    }

    @CallSuper
    @Override
    public void onDestroy() {
        compositeDisposable.clear();
    }
}
