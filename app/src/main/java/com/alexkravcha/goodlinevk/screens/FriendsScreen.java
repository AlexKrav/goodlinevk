package com.alexkravcha.goodlinevk.screens;

import androidx.fragment.app.Fragment;

import com.alexkravcha.goodlinevk.mvp.friends.FriendsFragment;
import com.alexkravcha.goodlinevk.navigation.SupportAppScreen;

public class FriendsScreen extends SupportAppScreen {

    public FriendsScreen() {
    }

    @Override
    public Fragment getFragment() {
        return FriendsFragment.newInstance();
    }
}
