package com.alexkravcha.goodlinevk.screens;

import androidx.fragment.app.Fragment;

import com.alexkravcha.goodlinevk.models.Friend;
import com.alexkravcha.goodlinevk.mvp.details.DetailsFragment;
import com.alexkravcha.goodlinevk.navigation.SupportAppScreen;

public class DetailsScreen extends SupportAppScreen {
    private Friend friend;

    public DetailsScreen(Friend friend) {
        this.friend = friend;
    }

    @Override
    public Fragment getFragment() {
        return DetailsFragment.newInstance(friend);
    }

}
