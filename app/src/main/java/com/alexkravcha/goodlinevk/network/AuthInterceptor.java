package com.alexkravcha.goodlinevk.network;

import com.alexkravcha.goodlinevk.CurrentUser;
import com.alexkravcha.goodlinevk.consts.C;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        HttpUrl url = request.url().newBuilder()
                .addQueryParameter(C.ApiConstants.ACCESS_TOKEN, CurrentUser.getAccessToken())
                .addQueryParameter(C.ApiConstants.V, C.ApiConstants.VERSION).build();
        request = request.newBuilder().url(url).build();
        return chain.proceed(request);
    }
}
