package com.alexkravcha.goodlinevk.network.interfaces;

import com.alexkravcha.goodlinevk.consts.C;
import com.alexkravcha.goodlinevk.models.BaseResponse;
import com.alexkravcha.goodlinevk.models.FriendsResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GetFriends {
    @GET(C.URLS.GET_FRIENDS)
    Single<BaseResponse<FriendsResponse>> getFriends(@Query("user_id") Integer userId,
                                                     @Query("fields") String nameCase);

}
