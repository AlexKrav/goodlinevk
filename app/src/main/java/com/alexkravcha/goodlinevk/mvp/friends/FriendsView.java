package com.alexkravcha.goodlinevk.mvp.friends;

import com.alexkravcha.goodlinevk.base.ViewBase;
import com.alexkravcha.goodlinevk.models.Friend;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

public interface FriendsView extends ViewBase {
    @StateStrategyType(OneExecutionStateStrategy.class)
    void startSignIn();

    void signedId();

    void showFriendsList(List<Friend> friendList);

    void showError(String error);
}
