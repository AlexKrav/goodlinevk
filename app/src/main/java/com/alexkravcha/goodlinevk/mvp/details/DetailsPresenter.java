package com.alexkravcha.goodlinevk.mvp.details;

import com.alexkravcha.goodlinevk.App;
import com.alexkravcha.goodlinevk.base.PresenterBase;
import com.alexkravcha.goodlinevk.base.ViewBase;
import com.alexkravcha.goodlinevk.models.Friend;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;

@InjectViewState
public class DetailsPresenter extends PresenterBase<DetailsView> {
    private Friend friend;

    @Inject
    Router router;

    DetailsPresenter(Friend friend) {
        App.getApplicationComponent().inject(this);
        this.friend = friend;
    }

    @Override
    protected void onFirstViewAttach() {
        getViewState().showFriendDetails(friend);
    }

    void onBackPressed() {
        router.exit();
    }
}
