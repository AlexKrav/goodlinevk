package com.alexkravcha.goodlinevk.mvp.main;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.alexkravcha.goodlinevk.adapters.FriendsAdapter;
import com.alexkravcha.goodlinevk.base.BaseActivity;
import com.alexkravcha.goodlinevk.CurrentUser;
import com.alexkravcha.goodlinevk.R;
import com.alexkravcha.goodlinevk.consts.C;
import com.alexkravcha.goodlinevk.models.Friend;
import com.alexkravcha.goodlinevk.mvp.friends.FriendsFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import java.util.List;

import butterknife.BindView;

public class MainActivity extends BaseActivity implements MainView {

    @InjectPresenter
    MainActivityPresenter presenter;


    @Override
    public int provideLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                Fragment friendsFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                friendsFragment.onActivityResult(requestCode, resultCode, data);
                Log.d("TAAG", "onResult()");
            }

            @Override
            public void onError(VKError error) {
                Log.d("TAAG", "onError()");
                // User didn't pass Authorization
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        presenter.onBackPressed();
    }
}
