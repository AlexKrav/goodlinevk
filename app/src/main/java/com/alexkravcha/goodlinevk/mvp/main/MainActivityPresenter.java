package com.alexkravcha.goodlinevk.mvp.main;

import com.alexkravcha.goodlinevk.App;
import com.alexkravcha.goodlinevk.CurrentUser;
import com.alexkravcha.goodlinevk.models.BaseResponse;
import com.alexkravcha.goodlinevk.network.NetworkClient;
import com.alexkravcha.goodlinevk.network.interfaces.GetFriends;
import com.alexkravcha.goodlinevk.screens.FriendsScreen;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.Objects;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class MainActivityPresenter extends MvpPresenter<MainView> {
    @Inject
    Router router;

    MainActivityPresenter() {
        App.getApplicationComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        router.newRootScreen(new FriendsScreen());
    }

    public void onBackPressed() {
        router.exit();
    }
}
