package com.alexkravcha.goodlinevk.mvp.friends;

import com.alexkravcha.goodlinevk.App;
import com.alexkravcha.goodlinevk.CurrentUser;
import com.alexkravcha.goodlinevk.base.PresenterBase;
import com.alexkravcha.goodlinevk.consts.C;
import com.alexkravcha.goodlinevk.models.BaseResponse;
import com.alexkravcha.goodlinevk.models.Friend;
import com.alexkravcha.goodlinevk.network.NetworkClient;
import com.alexkravcha.goodlinevk.network.interfaces.GetFriends;
import com.alexkravcha.goodlinevk.screens.DetailsScreen;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class FriendsFragmentPresenter extends PresenterBase<FriendsView> {

    @Inject
    NetworkClient networkClient;

    @Inject
    Router router;

    private List<Friend> searchFriendList = new ArrayList<>();
    private List<Friend> friendList = new ArrayList<>();

    FriendsFragmentPresenter() {
        App.getApplicationComponent().inject(this);
    }

    void checkAuth() {
        if (!CurrentUser.isAuthorized()) {
            getViewState().startSignIn();
        } else {
            getFriendsList();
            getViewState().signedId();
        }
    }

    void getFriendsList() {
        disposeOnDestroy(networkClient.with(GetFriends.class).getFriends(Integer.parseInt(Objects.requireNonNull(CurrentUser.getId())), C.ApiConstants.NAME_CASE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(BaseResponse::getResponse)
                .subscribe(response -> {
                    this.friendList = response.getFriends();
                    getViewState().showFriendsList(response.getFriends());
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }

    void onFriendClicked(Friend friend) {
        router.navigateTo(new DetailsScreen(friend));
    }

    void onTextChange(String editable) {
        searchFriendList.clear();
        if (editable.length() == 0) {
            getViewState().showFriendsList(friendList);
        } else {
            for (Friend item : friendList) {
                String currentItemTextCompanyTitle = item.getFirstName().toLowerCase();
                String currentItemTextAddress = item.getLastName().toLowerCase();
                if (currentItemTextCompanyTitle.contains(editable) || currentItemTextAddress.contains(editable)) {
                    searchFriendList.add(item);
                }
            }

        }
    }

    List<Friend> getSearchedFriends() {
        return searchFriendList;
    }
}
