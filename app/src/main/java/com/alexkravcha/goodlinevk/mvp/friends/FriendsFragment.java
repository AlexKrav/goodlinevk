package com.alexkravcha.goodlinevk.mvp.friends;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.alexkravcha.goodlinevk.CurrentUser;
import com.alexkravcha.goodlinevk.R;
import com.alexkravcha.goodlinevk.adapters.FriendsAdapter;
import com.alexkravcha.goodlinevk.base.FragmentBase;
import com.alexkravcha.goodlinevk.consts.C;
import com.alexkravcha.goodlinevk.models.Friend;
import com.alexkravcha.goodlinevk.view.SearchFriendView;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vk.sdk.VKSdk;

import java.util.List;

import butterknife.BindView;

public class FriendsFragment extends FragmentBase implements FriendsView, FriendsAdapter.OnFriendClickListener {

    @BindView(R.id.rvFriends)
    RecyclerView rvFriends;

    @BindView(R.id.searchView)
    SearchFriendView searchView;

    @BindView(R.id.friendContainer)
    ConstraintLayout container;

    @BindView(R.id.tvNoResult)
    TextView tvNoResult;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private FriendsAdapter friendsAdapter = new FriendsAdapter(this);

    @InjectPresenter
    FriendsFragmentPresenter presenter;

    public static FriendsFragment newInstance() {
        return new FriendsFragment();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        presenter.checkAuth();
        rvFriends.setLayoutManager(new LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false));
        rvFriends.setAdapter(friendsAdapter);

        setupListeners();
    }

    private void setupListeners() {

        swipeRefreshLayout.setOnRefreshListener(() -> {
            presenter.getFriendsList();
        });

        searchView.setOnSearchFriendViewListener(new SearchFriendView.OnSearchFriendViewListener() {
            @Override
            public void onTextChange(String editable) {
                presenter.onTextChange(editable);
            }

            @Override
            public void onDeleteTextCLicked() {
                searchView.clearText();
            }

            @Override
            public void onCancelClicked() {
                searchView.clearFocus();
                searchView.clearText();
                InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
            }

            @Override
            public void onSearchClicked() {
                List<Friend> friends = presenter.getSearchedFriends();
                if (searchView.getText().length() != 0) {
                    showFriendsList(friends);
                }
            }
        });
    }

    @Override
    protected int provideLayoutResId() {
        return R.layout.fragment_friends;
    }

    @Override
    public void startSignIn() {
        VKSdk.login(requireActivity(), C.ApiConstants.DEFAULT_LOGIN_SCOPE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        presenter.checkAuth();
    }

    @Override
    public void signedId() {
        searchView.focused();
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(searchView, 0);
    }

    @Override
    public void showFriendsList(List<Friend> friendList) {
        if (friendList.size() != 0) {
            friendsAdapter.replace(friendList);
            tvNoResult.setVisibility(View.INVISIBLE);
            rvFriends.setVisibility(View.VISIBLE);
            swipeRefreshLayout.setRefreshing(false);
        } else {
            tvNoResult.setVisibility(View.VISIBLE);
            rvFriends.setVisibility(View.INVISIBLE);

        }
    }

    @Override
    public void showError(String error) {
        Toast.makeText(requireContext(), error, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onFriendClicked(Friend friend) {
        presenter.onFriendClicked(friend);
    }
}
