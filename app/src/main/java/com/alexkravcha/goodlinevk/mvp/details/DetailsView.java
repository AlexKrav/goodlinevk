package com.alexkravcha.goodlinevk.mvp.details;

import com.alexkravcha.goodlinevk.base.ViewBase;
import com.alexkravcha.goodlinevk.models.Friend;
import com.arellomobile.mvp.MvpView;

public interface DetailsView extends ViewBase {
    void showFriendDetails(Friend friend);
}
