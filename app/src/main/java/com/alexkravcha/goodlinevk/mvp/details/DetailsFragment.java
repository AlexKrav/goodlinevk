package com.alexkravcha.goodlinevk.mvp.details;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.ImageViewCompat;
import androidx.databinding.DataBindingUtil;

import com.alexkravcha.goodlinevk.R;
import com.alexkravcha.goodlinevk.base.FragmentBase;
import com.alexkravcha.goodlinevk.databinding.FragmentDetailsBinding;
import com.alexkravcha.goodlinevk.models.Friend;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import butterknife.BindView;

public class DetailsFragment extends FragmentBase implements DetailsView {
    @BindView(R.id.online)
    TextView tvOnline;

    @BindView(R.id.name)
    TextView tvFullName;

    @BindView(R.id.btnBack)
    ConstraintLayout btnBack;

    @BindView(R.id.firstName)
    TextView tvFirstName;

    @BindView(R.id.lastName)
    TextView tvLastName;

    @BindView(R.id.screename)
    TextView tvScreeName;

    @BindView(R.id.sex)
    TextView tvSex;

    @BindView(R.id.city)
    TextView tvCity;

    @BindView(R.id.photo)
    ImageView photo;

    @InjectPresenter
    DetailsPresenter presenter;

    public static DetailsFragment newInstance(Friend friend) {
        Bundle args = new Bundle();
        args.putSerializable("friend", friend);
        DetailsFragment fragment = new DetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int provideLayoutResId() {
        return R.layout.fragment_details;
    }

    @ProvidePresenter
    DetailsPresenter providePresenter() {
        Friend friend = null;
        if (getArguments() != null) {
            friend = (Friend) getArguments().getSerializable("friend");

        }
        return new DetailsPresenter(friend);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupListeners();
    }

    private void setupListeners() {
        btnBack.setOnClickListener(view -> presenter.onBackPressed());
    }

    @Override
    public void showFriendDetails(Friend friend) {
        // можно бы ло бы засетить датабиндингом, но вроде как начал делать баттернайфом
        tvFirstName.setText(friend.getFirstName());
        tvLastName.setText(friend.getLastName());
        tvFullName.setText(String.format("%s %s", friend.getFirstName(), friend.getLastName()));

        if (friend.getOnline() == 0) {
            tvOnline.setText(getResources().getText(R.string.online));
        } else tvOnline.setText(getResources().getText(R.string.offline));

        if (!friend.getNickname().isEmpty()) {
            tvScreeName.setText(friend.getNickname());
        } else tvScreeName.setText(getResources().getText(R.string.no_choose));

        if (friend.getCity() != null) {
            tvCity.setText(friend.getCity());
        } else tvCity.setText(getResources().getText(R.string.no_choose));

        switch (friend.getSex()) {
            case 0:
                tvSex.setText(getResources().getText(R.string.no_choose));
                break;
            case 1:
                tvSex.setText(getResources().getString(R.string.female));
                break;
            case 2:
                tvSex.setText(getResources().getString(R.string.male));
                break;
        }

        Glide.with(this)
                .load(friend.getPhotoOrig())
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .thumbnail(Glide.with(this)
                        .load(friend.getPhotoLink50()))
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .into(photo);
    }
}
