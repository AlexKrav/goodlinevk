package com.alexkravcha.goodlinevk.view;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;

import com.alexkravcha.goodlinevk.R;

public class SearchFriendView extends FrameLayout {

    private AppCompatEditText textInput;
    private AppCompatImageView ivSearch, ivDeleteText;
    private TextView cancel;

    private OnSearchFriendViewListener onSearchFriendViewListener;

    public void setOnSearchFriendViewListener(OnSearchFriendViewListener onSearchFriendViewListener) {
        this.onSearchFriendViewListener = onSearchFriendViewListener;
    }

    public SearchFriendView(@NonNull Context context) {
        super(context);
    }

    public SearchFriendView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public SearchFriendView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    public SearchFriendView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    void init(AttributeSet attrs) {
        inflate(getContext(), R.layout.search_view, this);
        textInput = findViewById(R.id.edSearch);
        ivSearch = findViewById(R.id.icSearch);
        ivDeleteText = findViewById(R.id.icDeleteText);
        cancel = findViewById(R.id.tvCancel);

        setupListeners();
    }

    private void setupListeners() {

        ivDeleteText.setOnClickListener(view -> onSearchFriendViewListener.onDeleteTextCLicked());

        cancel.setOnClickListener(view -> onSearchFriendViewListener.onCancelClicked());

        ivSearch.setOnClickListener(view -> onSearchFriendViewListener.onSearchClicked());

        textInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                onSearchFriendViewListener.onTextChange(editable.toString().toLowerCase());
            }
        });
    }

    public void clearText() {
        Editable currentText = textInput.getText();
        if (currentText != null) {
            textInput.getText().clear();
        }
    }

    public String getText() {
        return String.valueOf(textInput.getText());
    }

    public void clearFocus() {
        textInput.clearFocus();
    }

    public void focused() {
        textInput.requestFocus();
    }

    public interface OnSearchFriendViewListener {
        void onTextChange(String editable);

        void onDeleteTextCLicked();

        void onCancelClicked();

        void onSearchClicked();
    }

}
