package com.alexkravcha.goodlinevk.consts;

public class C {
    public static class ApiConstants {
        public static final String[] DEFAULT_LOGIN_SCOPE = {};
        public static final String ACCESS_TOKEN = "access_token";
        public static final String V = "v";
        public static final String VERSION = "5.52";
        public static final String NAME_CASE = "photo_50,photo_100,photo_200_orig,relation,nickname,sex,domain";
    }

    public static class URLS {
        public static final String VK_BASE_URL = "https://api.vk.com/";
        public static final String api = "%s/method/";
        public static final String GET_FRIENDS = "friends.get";
    }
}
