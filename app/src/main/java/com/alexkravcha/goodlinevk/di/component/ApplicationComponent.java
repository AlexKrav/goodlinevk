package com.alexkravcha.goodlinevk.di.component;

import com.alexkravcha.goodlinevk.base.BaseActivity;
import com.alexkravcha.goodlinevk.di.module.ApplicationModule;
import com.alexkravcha.goodlinevk.di.module.NavigationModule;
import com.alexkravcha.goodlinevk.di.module.RestModule;
import com.alexkravcha.goodlinevk.mvp.details.DetailsPresenter;
import com.alexkravcha.goodlinevk.mvp.friends.FriendsFragmentPresenter;
import com.alexkravcha.goodlinevk.mvp.main.MainActivityPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, RestModule.class, NavigationModule.class})
public interface ApplicationComponent {

    void inject(BaseActivity baseActivity);

    void inject(MainActivityPresenter mainActivityPresenter);

    void inject(FriendsFragmentPresenter friendsFragmentPresenter);

    void inject(DetailsPresenter detailsPresenter);
}
