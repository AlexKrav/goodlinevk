package com.alexkravcha.goodlinevk.di.module;

import com.alexkravcha.goodlinevk.network.NetworkClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RestModule {
    private NetworkClient networkClient;

   public RestModule() {
        networkClient = new NetworkClient();
    }

    @Singleton
    @Provides
    public NetworkClient provideNetworkClient() {
        return networkClient;
    }
}
